# Learning Process

### Q1. What is the Feynman Technique? Explain in 1 line.

The Feynman Technique is a method for learning or reviewing a concept quickly by explaining it in plain, simple language.


### Q2. In this video, what was the most interesting story or idea for you?

The idea of the "Pomodoro Technique" discussed in the video is quite interesting and helpful to me. The idea is to switch between the "Focus Mode" and "Diffuse Mode" periodically which is somewhat related to the techniques that Salvador Dali and Thomas Edison have used.

### Q3. What are active and diffused modes of thinking?

There are two modes in which our mind works. "Active Mode" and "Diffuse Mode". Active mode is also called "Focus Mode". As the name suggests, in this mode the mind works by focusing on one thing only. In diffuse mode, the mind works loosely on one thing and the things around it.


### Q4. According to the video, what are the steps to take when approaching a new topic? Only mention the points.

1. Deconstruct the skill
2. Learn enough to self-correct
3. Remove practice barriers
4. Practice at least 20 hours


## References
[How to Use the Feynman Technique to Learn Faster (With Examples) - Online Article](https://collegeinfogeek.com/feynman-technique/)

[Learning how to learn | Barbara Oakley - YouTube Video](https://www.youtube.com/watch?v=O96fE1E-rf8&ab_channel=TEDxTalks)

[The first 20 hours -- how to learn anything | Josh Kaufman - YouTube Video](https://www.youtube.com/watch?v=5MgBikgcWnY&ab_channel=TEDxTalks)