# Tiny Habits

### Q1. In this video, what was the most interesting story or idea for you?

The idea of making an existing behavior to make trigger a new tiny habit is most interesting. "After I <u>(existing habit)</u>, I will <u>(new tiny habit)</u>". For Example, "After I go to bed, I'll read four pages of the book". It will help people develop tiny habits they tend to forget in the starting days.

### Q2. How can you use B = MAP to make making new habits easier? What are M, A and P.

According to BJ Fogg, B = MAP means that any behavior(B) of a person directly depends upon the motivation(M) to complete the task, the ability(A) to do the task, and the prompt(P) to start the task. The motivation to do a tiny task is much higher than to do a big task, so we should shrink any habit we want to develop into a tiny habit to make it a habit for the long term. After we develop a tiny habit, make it big day by day and make it shine.


### Q3. Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

"Shine" is the term BJ Fogg created to explain the feeling of someone after an accomplishment. Celebrating even after a tiny habit task completion is very important as it increases the confidence and motivation to the things and it creates a success momentum.


### Q4. In this video, what was the most interesting story or idea for you?

The statement "Every action you take is a vote for the type of person you want to become", I found most interesting. It means that if you want to become someone new, then make a new action a habit for you to accumulate it. Anything can be done if you develop a habit of doing things.


### Q5. What is the book's perspective about Identity?

If a habit becomes a part of identity then it gives the ultimate form of motivation to things. To solve any problems in the long term, we need to make changes in our identity.

### Q6. Write about the book's perspective on how to make a habit easier to do?

A habit becomes hard due to procrastination. To make a habit easy, we should make it more accessible and easy to start. Keep the things to get started with the habit as close as possible. Put very more steps between you and the bad behaviors and fewer steps between you and the good behaviors.

### Q7. Write about the book's perspective on how to make a habit harder to do?

If you put more steps between you and your habit, then it becomes harder to do a habit. Procrastination is the main factor to becomes a habit harder to do. Making things inaccessible or accessible with some effort makes it hard to start the habit to do.

### Q8. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

The one habit I want to do more of is "Communication Practice". I lack English communication skills and I want to improve it by practicing daily. I can read books, watch movies, and practice with my friends to improve my English communication skills.

To make the cue obvious I'll do it every day after going to bed before sleeping. To make it more attractive, I can watch English movies and have funny conversations with my friends in English.

### Q9. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

One habit I would like to eliminate or do less is "Less Sleeping". This is one of my worst habits and it causes many problems in the workspace. I hardly sleep for 5-6 hours and feel sleepy in the workspace. 

To make the cue invisible I would keep my phone(electronics) away 1-2 hours before going to bed. It would help to focus on sleeping instead of scrolling my phone/laptop aimlessly.