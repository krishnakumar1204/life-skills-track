# Good Practices for Software Development

### Q1. Which point(s) were new to you?

These points were new to me:
* Sharing code snippets with Github gists and entire setups with sandbox environments.
* Tracking time using apps to improve productivity.


### Q2. Which area do you think you need to improve on? What are your ideas to make progress in that area?

These are the areas which I need to improve on:
- **Communication:** I struggle a bit to communicate in the English language. These are the ideas to make a progress in this:
    1. Practice speaking English daily for at least 30 minutes.
    2. Talking to my friends in English.

- **Deep Work:** I couldn't focus on the work longer. I am working on it to improve. Here are the ideas to make a progress in this:
    1. Shutting down my phone when I am working.
    2. Working in the less distracting zone.
    3. Enjoy the process of working.