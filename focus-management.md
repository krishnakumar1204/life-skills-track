# Focus Management


### Q1. What is Deep Work?
Deep Work means focusing on a cognatively demanding task without any distraction.

### Q2. According to author how to do deep work properly, in a few points?

According to the author, we should keep the below points in mind to do deep work:
* Schedule your distractions at home and work.
* Develop a rhythmic deep work ritual.
* Develop a daily complete shutdown ritual.


### Q3. How can you implement the principles in your day to day life?
To implement the above principles I can do these things:
* Schedule one or two 10-20 minutes blocks for the distractions.
* Fix the timing of the Deep Work for every day.
* Cut down completely from the work in the evening.
* And most importantly getting enough sleep.

### Q4. What are the dangers of social media, in brief?

* It can be addictive which increases day by day.
* It reduces our ability to focus and concentrate.
* It reduces our productivity.
* It can lead to feeling of inadeaquacy and loneliness.
* It can cause anxiety.