# Grit and Growth Mindset

### Q1. Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

The one characteristic that significantly predicts success is 'Grit'. Grit is passion and perseverance for long-term goals. The growth mindset is a great idea to build Grit.


### Q2. Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

The core idea behind the growth mindset is that we can learn and improve any skill. The main two characteristics of the growth mindset are Beliefs and Focus. Believing that we can learn and improve and focusing on the process instead of the outcome can build a growth mindset.


### Q3. What is the Internal Locus of Control? What is the key point in the video?

The internal locus of control means that we are the controller of our own destiny. It is the main factor to stay motivated all the time. 

These are the key points from the video:
* People with an internal locus of control believe that their actions and effort determine their outcomes but people with an external focus of control believe that the factors outside determines the outcome.
* Having an internal locus of control is key to staying motivated.
* People who believe they have no control over their outcomes are less likely to be motivated.
* The best way to develop an internal locus of control is to solve problems in your own life and attribute your success to your efforts.

### Q4. What are the key points mentioned by speaker to build growth mindset (explanation not needed).

The key points to build growth mindset is:
* Believe in your abilty to figure things out.
* Question your assumptions.
* Develop your own life curriculum.
* Honour your struggle.

### Q4. What are your ideas to take action and build Growth Mindset?

To build a growth mindset the main key point is to believe that any skill can be learned or improved. When i see any challanges, i see them as an oppurtunity to learn and get better in that. I believe that this is in my control ans i can do it.