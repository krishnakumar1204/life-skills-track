# Data Warehousing


Data warehousing is the process of collecting, storing, and managing data from various sources into a central repository to provide meaningful insights about the data. This repository is called the Data Warehouse. These data are mainly used for querying and analysis. Based on this analysis of these data, we extract meaningful information about the business, and these informations help to grow the business.


It is a mixture of technologies and components that helps to use data strategically. Instead of transaction processing, it is the automated collection of a vast amount of information by a company that is configured for demand and review. It’s a process of transforming data into information and making it available for users to make a difference in a timely way.




## Characteristics of Data Warehousing


### 1. Subject-Oriented


Data warehousing is a subject-oriented process, as we can extract information about a specific subject rather than an operation of an organization.



### 2. Integerated


Date warehouses should be integrated with standard units of measurement to implement similar data from different databases.



### 3. Time-varient


Data warehouses are time-varient as they store data for a given period of time.



### 4. Non-volatile


The data warehouse is non-volatile as it stores the prior data as well.





## Key Components of Data Warehousing


### 1. Data Sources
In data warehousing, there must be one (usually more than one) data source to provide data.



### 2. ETL (Extract, Transform, Load)
The ETL process is at the heart of data warehousing. It involves three main stages:


- **Extract:** Data is extracted from source systems and transferred to the data warehouse.
- **Transform:** Data is cleansed, transformed, and restructured to fit the warehouse’s schema and business rules.
- **Load:** Transformed data is loaded into the data warehouse for analysis.



### 3. Data Warehouse


The data warehouse is the central repository to store the data on which analysis is done.



### 4. Business Intelligence Tools
These tools are used to analyze the data and extract information from it. Examples: Power BI, etc.


## Refrences
[Medium Article 1](https://medium.com/@rashmilis1/understanding-data-warehousing-concepts-for-business-analysts-f00950884584)

[Medium Article 2](https://medium.com/datatobiz/a-complete-guide-to-data-warehousing-what-is-data-warehousing-its-architecture-characteristics-863220d605d6)