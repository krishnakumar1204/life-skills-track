# Listening and Assertive Communication

### Q1. What are the steps/strategies to do Active Listening? (Minimum 6 points)

1. Avoid getting distracted by your own thoughts
2. Try not to interrupt the other person
3. Use door openers
4. Show that you are listening with your body language
5. If appropriate, take notes during important conversation
6. Paraphrase what others have said to make sure you are on the same page.


### Q2. According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

Reflecting listening is a strategy in communication that is based on the idea of confirming the things that the speaker is saying by different methods. According to Fisher's Model, these are the key points of reflecting listening:

* **Repeating:** - This means restating the same thing that the speaker has said in your own words.
* **Paraphrasing:** - This means rephrasing the content that the speaker has said to confirm the same.
* **Clarifying:** - Clarifying things by asking questions.
* **Empathizing:** - Understanding the feelings and perspective of the speaker.


### Q3. What are the obstacles in your listening process?

There can be reasons to hinder my listening process based on the situation and state I am in. Some of them are:

* Sometimes during the listening process the mind diverts to the different thoughts that are revolving in the mind for a long time.
* If I have stress of any kind then it can be an obstacle in the listening process as I won't be able to focus on the things that the speaker is saying.


### Q4. What can you do to improve your listening?

To improve my listening skills, I can leave the ideas that are not related to the current content I am listening to and try to focus on the current things. 


### Q5. When do you switch to Passive communication style in your day to day life?

I use my passive communication style in cases where I see something can go wrong by this and cause some conflict in the team. In those cases where the stakes are low.


### Q6. When do you switch into Aggressive communication styles in your day to day life?

In my day-to-day life, I do not use aggressive communication as I do not need it. All the communication can be done using an assertive communication style. But it can be used in the rare situation where things get out of control.


### Q7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

I also do not use Passive Aggressive communication style in my day-to-day life as it can be very dangerous most of the time. I prefer to talk directly and point to point. 


### Q8. How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? 

Assertive communication style is one of the best kind of communication style. It can be very helpful in most of the situations. We can make our communication assertive by practicing and keeping these things in mind during conversation:

* Recognize and name your feelings
* Recognize and name what you need
* Start with low-stakes situations.
* Be aware of your body language.

In my case, if I have to make my communication style assertive, then I try to be direct and point to point taking care of my needs and the needs of others.


## References
[Active Listening - YouTube Video](https://www.youtube.com/watch?v=rzsVh8YwZEQ&ab_channel=LearnFree)

[Wikipedia Article on Reflective listening](https://en.wikipedia.org/wiki/Reflective_listening)

[Fisher's Model of reflective listening](http://www.analytictech.com/mb119/reflecti.htm)

[How passive communication leads to aggressive communication - YouTube Video](https://www.youtube.com/watch?v=BanqlGZSWiI&ab_channel=HowtoADHD)

[5 Tips to Make Assertive Communication Easier and More Effective - YouTube Video](https://www.youtube.com/watch?v=vlwmfiCb-vc&ab_channel=HowtoADHD)