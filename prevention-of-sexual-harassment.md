# Prevention of sexual harassment

### Q. What kinds of behaviour cause sexual harassment?

### Ans: 
Any kind of behaviour which makes a person feel uncomfortable at the workplace is sexual harassment. If any person crosses the personal and physical boundary of a person then it is sexual harassment. Some of the behaviours that are sexual harassment are:
* repeated compliments of an employee's appearance
* commenting on the attractiveness of others in front of an employee
* discussing one's sex life in front of an employee
* asking an employee about his or her sex life
* circulating nude photos or photos of women in bikinis or shirtless men in the workplace
* making sexual jokes
* sending sexually suggestive text messages or emails
* leaving unwanted gifts of a sexual or romantic nature
* spreading sexual rumors about an employee, or
* repeated hugs or other unwanted touching (such as a hand on an employee's back).

There are many more behaviours that are sexual harassment. These behaviours can be broadly classified into two categories:
1. Quid pro quo harassment
2. Hostile Work Environment



### Q. What would you do in case you face or witness any incident or repeated incidents of such behaviour?

### Ans:
If I face or witness any incident or repeated incidents of this kind of behaviour the first thing I will do is stop the person who is doing it. If the person does not stop or I feel uncomfortable telling them to stop then I will report it to the supervisor/manager. If these behaviors are done by the supervisor, in that case, I will report it to their supervisor/manager or any person up in the chain of command with whom I am comfortable. In the most extreme case if the employer does not take any action or the person does not stop then I can go to the Police to file an FIR.

### Refrences
["Employment Sexual Harassment" video by "Cordua Training Videos"](https://www.youtube.com/watch?v=o3FhoCz-FbA&ab_channel=CorduaTrainingVideos)

[Online Article](https://www.nolo.com/legal-encyclopedia/what-kinds-of-behaviors-are-considered-sexual-harassment.html)