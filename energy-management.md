# Energy Management

### Q1. What are the activities you do that make you relax - Calm quadrant?

To get back to the relax-calm quadrant, I watch movies, listen to songs, shower, walk, or talk to my family. These are the activities that get me back to the relax-calm mode.


### Q2. When do you find getting into the Stress quadrant?
When I get some new tasks without enough resources or proper guidelines, with a tight deadline, it gets me to the stress quadrant. This happens for a short period until I start working on the task by figuring out the ways to tackle the difficulties to complete the task.


### Q3. How do you understand if you are in the Excitement quadrant?
When I feel curious about something, get surprises, or feel excited for something new to happen, I am in the Excitement quadrant. In this case, I feel my heart beating fast, a rush of energy, and a sense of anticipation.


### Q4. Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

According to the video, here are the key points about sleep:

* Lack of sleep can impair memory and learning.
* Sleep deprivation can negatively affect the immune system.
* Short sleep duration is linked to an increased risk of health problems.
* People who sleep less than 7 hours a night may have lower testosterone levels.



### Q5. What are some ideas that you can implement to sleep better?
These are the things which I can do to sleep better:

* No Screen-time before sleep.
* Regular exercise
* Fixed sleep schedule
* Fixed diet schedule

### Q6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

* Exercise is the most transformative thing to do for the brain.

* The immediate effect of exercise is that it improves mood, ability to focus and attention, and reaction time.

* If we do regular exercises then the hippocampus area of our brain produces new cells and gets bigger.

* Exercises protect from various types of diseases.

* The minimum amount of exercise one should do is 3-4 times a week, at least 30 minutes.

### Q7. What are some steps you can take to exercise more?
Things I do to exercise more:
* Make the exercises enjoyable by including fun activity.
* Exercise with friends.
* Exercise for a fixed period of time daily.
* Make the exercise rewarding.